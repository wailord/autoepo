import pathlib
import xml.etree.ElementTree as ET
from dataclasses import dataclass
from datetime import datetime

from autoepo.invoices import InvoiceEntry, Invoice
from autoepo.invoices.invoices import InvoicesAggregation


@dataclass
class Expense(InvoicesAggregation):
    pass


def parse_invoice(xml_data):
    # Define the namespace dictionary for XML parsing
    namespace = {'ns': 'http://isdoc.cz/namespace/2013'}

    # Parse the XML file
    root = ET.fromstring(xml_data)

    # Fetch the necessary elements
    invoices_id = root.find('ns:ID', namespace).text if root.find('ns:ID', namespace) is not None else ''
    customer_id = root.find('.//ns:AccountingCustomerParty/ns:Party/ns:PartyIdentification/ns:ID',
                            namespace).text if root.find(
        './/ns:AccountingCustomerParty/ns:Party/ns:PartyIdentification/ns:ID', namespace) is not None else ''

    tax_point_date = root.find('ns:TaxPointDate', namespace).text
    tax_point_date = datetime.strptime(tax_point_date, "%Y-%m-%d").date()

    # Initialize the Invoice data class
    invoice = Invoice(invoices_id=invoices_id, customer_id=customer_id, tax_point_date=tax_point_date)

    # Loop through each TaxSubTotal to gather entries
    for tax_subtotal in root.findall('ns:TaxTotal/ns:TaxSubTotal', namespace):
        tax_amount = float(tax_subtotal.find('ns:TaxAmount', namespace).text)
        amount = float(tax_subtotal.find('ns:TaxableAmount', namespace).text)
        tax_percent = float(tax_subtotal.find('.//ns:TaxCategory/ns:Percent', namespace).text)

        # Create an InvoiceEntry instance and append to the entries list
        invoice.entries.append(InvoiceEntry(tax_amount=tax_amount, amount=amount, tax_percent=tax_percent))

    return invoice

def parse_expense_folder(folder: pathlib.Path):
    invoices = []
    for file_path in folder.glob('*.isdoc'):
        with file_path.open('r', encoding='utf-8') as file:
            xml_content = file.read()
            invoice = parse_invoice(xml_content)
            invoices.append(invoice)
    return Expense(invoices)
