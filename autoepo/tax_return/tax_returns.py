import typing
import xml.etree.ElementTree as ET
from dataclasses import field, dataclass

from autoepo.datastorage.datastorage import Year, Month
from autoepo.user import User


@dataclass
class TaxReturn:
    user: User
    year: Year
    month: Month
    fee_bases: typing.Dict[float, float] = field(default_factory=dict)
    fee_values: typing.Dict[float, float] = field(default_factory=dict)
    tax_deduction_bases: typing.Dict[float, float] = field(default_factory=dict)
    tax_deduction_values: typing.Dict[float, float] = field(default_factory=dict)

    def write_to_file(self):
        tax_return = self
        root = ET.Element("Pisemnost", nazevSW="EPO MF ČR", verzeSW="44.2.1")
        dphdp3 = ET.SubElement(root, "DPHDP3", verzePis="02.01")

        # VetaD attributes (simplified here, populate from tax_return fields as required)
        ET.SubElement(dphdp3, "VetaD", c_okec="620000", d_poddp="22.02.2024", dapdph_forma="B", dokument="DP3",
                      k_uladis="DPH", mesic=str(tax_return.month.month), rok=str(tax_return.year.year), trans="A",
                      typ_platce="P")

        # VetaP attributes (using Address and User information)
        address = tax_return.user.address
        user = tax_return.user
        tax_office = user.tax_office

        ET.SubElement(dphdp3, "VetaP", c_orient=address.orientation_number, c_pop=address.registration_number,
                      c_telef=user.phone, dic=user.dic, email=user.email, jmeno=user.first_name,
                      naz_obce=address.municipality_name, prijmeni=user.surname, psc=address.postal_code,
                      stat=address.country, titul=user.degree, typ_ds="F", ulice=address.street,
                      c_pracufo=tax_office.working_office_number, c_ufo=tax_office.office_number)

        # Veta1 attributes
        fee_value = tax_return.fee_values.get(21, 0)
        fee_base = tax_return.fee_bases.get(21, 0)
        ET.SubElement(dphdp3, "Veta1", dan23=str(int(fee_value)), obrat23=str(int(fee_base)))

        # Veta4 attributes
        total_deduction_values = sum(tax_return.tax_deduction_values.values())
        specific_deduction_value = tax_return.tax_deduction_values.get(21, 0)
        specific_deduction_base = tax_return.tax_deduction_bases.get(21, 0)
        ET.SubElement(dphdp3, "Veta4", odp_sum_nar=str(int(total_deduction_values)),
                      odp_tuz23_nar=str(int(specific_deduction_value)), pln23=str(int(specific_deduction_base)))

        # Veta6 attributes (simplified here)
        ET.SubElement(dphdp3, "Veta6", dan_zocelk=str(int(fee_value)), dano="0",
                      dano_da=str(int(fee_value - total_deduction_values)), dano_no="0",
                      odp_zocelk=str(int(total_deduction_values)))

        # Generate XML string
        tree = ET.ElementTree(root)
        xml_str = ET.tostring(root, encoding='unicode', method='xml')
        tree.write(self.month.path() / "tax_return.xml", encoding='utf-8', xml_declaration=True)
