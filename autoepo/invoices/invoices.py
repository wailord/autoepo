import datetime
from dataclasses import dataclass, field


@dataclass
class InvoiceEntry:
    tax_amount: float
    amount: float
    tax_percent: float


@dataclass
class Invoice:
    invoices_id: str
    customer_id: str
    tax_point_date: datetime.date
    entries: list[InvoiceEntry] = field(default_factory=list)


@dataclass
class InvoicesAggregation:
    invoices: list[Invoice]

    @property
    def used_tax_percent(self):
        """Returns a set of all unique tax percentages used in the invoices."""
        return self._get_used_tax_percents()

    # @lru_cache(maxsize=None)
    def _get_used_tax_percents(self):
        tax_percents = set()
        for invoice in self.invoices:
            for entry in invoice.entries:
                tax_percents.add(entry.tax_percent)
        return tax_percents

    # @lru_cache(maxsize=None)
    def sum_of_tax_amount(self, tax_percent):
        """Returns the sum of tax amounts for a specific tax percentage across all invoices."""
        total_tax = 0
        for invoice in self.invoices:
            for entry in invoice.entries:
                if entry.tax_percent == tax_percent:
                    total_tax += entry.tax_amount
        return total_tax

    # @lru_cache(maxsize=None)
    def sum_of_amount(self, tax_percent):
        """Returns the sum of amounts for a specific tax percentage across all invoices."""
        total_amount = 0
        for invoice in self.invoices:
            for entry in invoice.entries:
                if entry.tax_percent == tax_percent:
                    total_amount += entry.amount
        return total_amount
