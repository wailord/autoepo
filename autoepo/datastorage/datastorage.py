import pathlib

from autoepo.expenses import parse_expense_folder
from autoepo.incomes import parse_income_folder
from autoepo.incomes.incomes import Income
from autoepo.utils import PathMixin


class Month(PathMixin):
    def __init__(self, data_storage, year, month):
        self._data = {}
        self.year = year
        self.month = month
        self._income = None
        self._expense = None
        self._tax_return = None
        self._control_statement = None
        self.data_storage: DataStorage = data_storage

    def path(self) -> pathlib.Path:
        return self.year.path() / f"{self.month:02}"

    @property
    def income(self) -> Income:
        if self._income is None:
            self._income = parse_income_folder(self.path() / "incomes")
        return self._income

    @property
    def expenses(self):
        if self._expense is None:
            self._expense = parse_expense_folder(self.path() / "expenses")
        return self._expense

    @property
    def tax_return(self):
        if self._tax_return is None:
            income = self.income
            expenses = self.expenses
            fee_values = {pct: income.sum_of_tax_amount(pct) for pct in income.used_tax_percent}
            fee_base = {pct: income.sum_of_amount(pct) for pct in income.used_tax_percent}
            tax_deduction_values = {pct: expenses.sum_of_tax_amount(pct) for pct in expenses.used_tax_percent}
            tax_deduction_bases = {pct: expenses.sum_of_amount(pct) for pct in expenses.used_tax_percent}
            from autoepo.tax_return import TaxReturn
            self._tax_return = TaxReturn(user=self.data_storage.user, year=self.year, month=self, fee_values=fee_values,
                                         fee_bases=fee_base, tax_deduction_bases=tax_deduction_bases,
                                         tax_deduction_values=tax_deduction_values)

        return self._tax_return
    @property
    def control_statement(self):
        if self._control_statement is None:
            from autoepo.control_statement import ControlStatement
            self._control_statement = ControlStatement(
                month=self,
                year=self.year,
                user=self.data_storage.user,
                incomes=self.income,
                expenses=self.expenses,
            )

        return self._control_statement


class Year(PathMixin):
    def __init__(self, datastorage, year):
        self.months = {}
        self.datastorage = datastorage
        self.year = year

    def path(self) -> pathlib.Path:
        return self.datastorage.path() / str(self.year)

    def __getitem__(self, month) -> Month:
        if month not in self.months:
            self.months[month] = Month(self.datastorage, self, month)
        return self.months[month]


class DataStorage(PathMixin):
    def __init__(self, user, path: pathlib.Path):
        self.user = user
        self.years = {}
        self._path = path

    def path(self) -> pathlib.Path:
        return self._path

    def __getitem__(self, year) -> Year:
        if year not in self.years:
            self.years[year] = Year(self, year)
        return self.years[year]
