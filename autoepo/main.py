import click
import pathlib

from autoepo.datastorage import DataStorage
from autoepo.user import parse_user_from_json


@click.command()
@click.option('--data_folder', type=click.Path(exists=True, file_okay=False))
@click.option('--month', type=int)
@click.option('--year', type=int)
@click.option('--output_folder', type=click.Path(file_okay=False))
def process_invoices(data_folder, month, year, output_folder):
    data_path = pathlib.Path(data_folder)

    with open(data_path / "user.json") as user_json:
        user_json = parse_user_from_json(user_json.read())

    output_path = pathlib.Path(output_folder)
    output_path.mkdir(parents=True, exist_ok=True)
    ds = DataStorage(user_json, data_path)
    tax_return = ds[year][month].tax_return
    tax_return.write_to_file()
    control_statement = ds[year][month].control_statement
    control_statement.write_to_file()

if __name__ == '__main__':
    process_invoices()
