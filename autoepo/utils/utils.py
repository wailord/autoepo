import pathlib
from abc import abstractmethod, ABC


class PathMixin(ABC):

    @abstractmethod
    def path(self) -> pathlib.Path:
        pass