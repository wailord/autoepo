import pathlib
import xml.etree.ElementTree as ET
from dataclasses import dataclass
from datetime import date

from autoepo.datastorage.datastorage import Year, Month
from autoepo.expenses import Expense
from autoepo.incomes.incomes import Income
from autoepo.user import User


@dataclass
class ControlStatement:
    user: User
    year: Year
    month: Month
    incomes: Income
    expenses: Expense

    def write_to_file(self):
        create_control_statement_xml(self, self.month.path() / "control_statement.xml")


def create_control_statement_xml(control_statement, file_name: pathlib.Path):
    # Create the root element
    root = ET.Element('Pisemnost', nazevSW="AUTOEPO MB", verzeSW="44.5.1")
    dphkh1 = ET.SubElement(root, 'DPHKH1', verzePis="03.01")

    # Generate VetaD element
    ET.SubElement(dphkh1, 'VetaD', dokument="KH1", k_uladis="DPH", mesic=str(control_statement.month.month),
                  rok=str(control_statement.year.year), d_poddp=date.today().strftime("%d.%m.%Y"), khdph_forma="B")

    # Assuming VetaP information is filled manually or from another source
    # Here, placeholder values are used.
    # ET.SubElement(dphkh1, 'VetaP', c_ufo=control_statement.user.tax_office.office_number,
    #               c_pracufo=control_statement.user.tax_office.working_office_number, dic="9311211789", typ_ds="F",
    #               titul="Ing.", jmeno="Michal", prijmeni="Bouška", ulice="Josefa Suka", c_orient="3", c_pop="1592",
    #               naz_obce="Most", psc="43401", stat="ČESKÁ REPUBLIKA", email="dane@bouska.eu", c_telef="+420736405478")

    user = control_statement.user
    address = user.address
    tax_office = user.tax_office

    ET.SubElement(dphkh1, "VetaP", c_orient=address.orientation_number, c_pop=address.registration_number,
                  c_telef=user.phone, dic=user.dic, email=user.email, jmeno=user.first_name,
                  naz_obce=address.municipality_name, prijmeni=user.surname, psc=address.postal_code,
                  stat=address.country, titul=user.degree, typ_ds="F", ulice=address.street,
                  c_pracufo=tax_office.working_office_number, c_ufo=tax_office.office_number)

    # Income details (VetaA4)
    c_radku = 1
    for invoice in control_statement.incomes.invoices:
        for entry in invoice.entries:
            if entry.tax_percent == 21:
                ET.SubElement(dphkh1, 'VetaA4', c_radku=str(c_radku), dic_odb=invoice.customer_id,
                              c_evid_dd=invoice.invoices_id, dppd=invoice.tax_point_date.strftime("%d.%m.%Y"),
                              zakl_dane1=str(entry.amount), dan1=str(entry.tax_amount), kod_rezim_pl="0", zdph_44="N")
                c_radku += 1
    income_total_23 = control_statement.incomes.sum_of_amount(21)

    # Expense details (VetaB2 and VetaB3)
    c_radku = 1
    total_amount = 0
    total_tax = 0
    for invoice in control_statement.expenses.invoices:
        invoice_total = sum(entry.amount for entry in invoice.entries if entry.tax_percent == 21)
        if invoice_total >= 10000:
            for entry in invoice.entries:
                if entry.tax_percent == 21:
                    ET.SubElement(dphkh1, 'VetaB2', c_radku=str(c_radku), dic_dod=invoice.customer_id,
                                  c_evid_dd=invoice.invoices_id, dppd=invoice.tax_point_date.strftime("%d.%m.%Y"),
                                  zakl_dane1=str(entry.amount), dan1=str(entry.tax_amount), pomer="N", zdph_44="N")
                    c_radku += 1
        else:
            total_amount += invoice_total
            total_tax += sum(entry.tax_amount for entry in invoice.entries if entry.tax_percent == 21)

    ET.SubElement(dphkh1, 'VetaB3', zakl_dane1=str(total_amount), dan1=str(total_tax))

    ET.SubElement(dphkh1, 'VetaC', obrat23=str(int(income_total_23)))

    # Convert the ElementTree to a string and save it to a file
    tree = ET.ElementTree(root)
    tree.write(file_name, encoding='utf-8', xml_declaration=True)
