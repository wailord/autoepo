from autoepo.incomes.incomes import parse_income_folder

__all__ = ["parse_income_folder"]
