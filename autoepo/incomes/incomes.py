import pathlib
import xml.etree.ElementTree as ET
from dataclasses import dataclass
from datetime import datetime

from autoepo.invoices import Invoice
from autoepo.invoices.invoices import InvoiceEntry, InvoicesAggregation


@dataclass
class Income(InvoicesAggregation):
    invoices: list[Invoice]


def parse_isdoc_bill(xml_data):
    # Parsing the XML with the correct namespace handling
    namespace = {'ns': 'http://isdoc.cz/namespace/2013'}
    root = ET.fromstring(xml_data)

    # Extracting the TaxPointDate
    tax_point_date = root.find('ns:TaxPointDate', namespace).text
    tax_point_date = datetime.strptime(tax_point_date, "%Y-%m-%d").date()

    invoices_id = root.find('ns:ID', namespace).text

    customer_id = root.find('.//ns:AccountingCustomerParty/ns:Party/ns:PartyIdentification/ns:ID', namespace).text

    # Initializing the Bill with the TaxPointDate
    bill = Invoice(tax_point_date=tax_point_date, invoices_id=invoices_id, customer_id=customer_id)

    # Looping through InvoiceLine elements
    invoice_lines = root.findall('.//ns:InvoiceLines/ns:InvoiceLine', namespace)
    for line in invoice_lines:
        tax_amount = float(line.find('.//ns:LineExtensionTaxAmount', namespace).text)
        amount = float(line.find('.//ns:LineExtensionAmount', namespace).text)
        tax_percent = float(line.find('.//ns:ClassifiedTaxCategory/ns:Percent', namespace).text)

        # Creating an Entry and adding it to the Bill
        entry = InvoiceEntry(tax_amount=tax_amount, amount=amount, tax_percent=tax_percent)
        bill.entries.append(entry)

    return bill


def parse_income_folder(path: pathlib.Path) -> Income:
    bills = []

    # Check if the path is a directory
    if not path.is_dir():
        raise ValueError(f"The path {path} is not a directory.")

    # Iterate over all XML files in the directory
    for file_path in path.glob('*.isdoc'):
        # Read the content of the file
        with open(file_path, 'r', encoding='utf-8') as file:
            xml_content = file.read()

        # Parse the XML content into a Bill object
        bill = parse_isdoc_bill(xml_content)
        bills.append(bill)

    return Income(bills)
