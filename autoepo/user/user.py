import json
from dataclasses import dataclass


@dataclass
class Address:
    orientation_number: str
    registration_number: str
    municipality_name: str
    postal_code: str
    country: str
    street: str


@dataclass
class TaxOffice:
    office_number: str
    working_office_number: str


@dataclass
class User:
    address: Address
    tax_office: TaxOffice
    first_name: str
    surname: str
    email: str
    phone: str
    degree: str
    dic: str  # VAT ID


def parse_user_from_json(json_data: str) -> User:
    data = json.loads(json_data)
    address_data = data.pop('address')
    address = Address(**address_data)
    office_data = data.pop('tax_office')
    tax_office = TaxOffice(**office_data)
    return User(**data, address=address, tax_office=tax_office)
